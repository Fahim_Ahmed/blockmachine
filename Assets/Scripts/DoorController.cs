﻿using UnityEngine;
using Light2D;

public class DoorController : MonoBehaviour {

    public bool isOpen = true;

    public Color openColor;
    public Color closeColor;


    private GameObject doorClosed;
    private LightSprite light;

    private void Awake()
    {
        light = transform.Find("Light").GetComponent<LightSprite>();

        doorClosed = transform.Find("DoorClosed").gameObject;
        doorClosed.SetActive(!isOpen);

        light.Color = isOpen ? openColor : closeColor;
    }


    public void OpenDoor(bool bOpen = true)
    {
        isOpen = bOpen;

        doorClosed.SetActive(!bOpen);
        light.Color = bOpen ? openColor : closeColor;
    }
}
