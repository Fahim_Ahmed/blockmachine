﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class PanelSwitcher : MonoBehaviour {

    public GameObject panelA;
    public GameObject panelB;

    public GameObject buttonA;
    public GameObject buttonB;

    public Color active;
    public Color deactive;
    public Color textActive;
    public Color textDeactive;

    public void SwitchPanel(int i)
    {
        if(i == 0)
        {
            panelA.SetActive(true);
            buttonA.GetComponent<Image>().color = active;
            buttonA.transform.GetChild(0).GetComponent<TMP_Text>().color = textActive;

            panelB.SetActive(false);
            buttonB.GetComponent<Image>().color = deactive;
            buttonB.transform.GetChild(0).GetComponent<TMP_Text>().color = textDeactive;

        }
        else
        {
            panelB.SetActive(true);
            buttonB.GetComponent<Image>().color = active;
            buttonB.transform.GetChild(0).GetComponent<TMP_Text>().color = textActive;

            panelA.SetActive(false);
            buttonA.GetComponent<Image>().color = deactive;
            buttonA.transform.GetChild(0).GetComponent<TMP_Text>().color = textDeactive;
        }
    }
}
