﻿using UnityEngine;
using UnityEngine.Events;

using TMPro;

public class EditorController : MonoBehaviour {

    public TMP_InputField[] editorLines;

    [SerializeField]
    public UnityEvent onRestore;

    private int currentLineIndex = 0;
    private bool bParameter = false;
    private bool bLoopStart = false;
    private int loopCount = 1;

    private void Awake()
    {
        foreach(TMP_InputField i in editorLines)
        {
            i.onEndEdit.AddListener(OnInputEditEnd);
        }

        //onRestore = new UnityEvent();
    }

    private void OnInputEditEnd(string s)
    {
        
    }

    public void ClearAll()
    {
        foreach (TMP_InputField i in editorLines) i.text = "";
        currentLineIndex = 0;
        bParameter = false;
        bLoopStart = false;
        loopCount = 0;

        onRestore.Invoke();
    }

    public void OnButtonPress(string func)
    {

        if (func == "null") return;

        if(func == "reset")
        {
            ClearAll();
            return;
        }

        if(currentLineIndex < editorLines.Length)
        {
            if (bParameter)
            {
                string txt = editorLines[currentLineIndex].text;

                if (txt.Contains("..."))
                {
                    editorLines[currentLineIndex].text = txt.Replace("...", func);
                }
                else
                {
                    editorLines[currentLineIndex].text += " " + func;
                    currentLineIndex++;
                    bParameter = false;
                }
            }
            else if (bLoopStart)
            {
                if (func == "loop")
                {
                    loopCount++;
                    editorLines[currentLineIndex].text = func + "(" + loopCount + ", ...)";

                    return;
                }

                editorLines[currentLineIndex].text = editorLines[currentLineIndex].text.Replace("...", func);
                currentLineIndex++;

                loopCount = 0;
                bLoopStart = false;
            }
            else
            {
                if (func == "if")
                {
                    editorLines[currentLineIndex].text = func + "(...)";
                    bParameter = true;
                }
                else if (func == "loop")
                {
                    editorLines[currentLineIndex].text = func + "(" + loopCount + ", ...)";
                    bLoopStart = true;
                }
                else
                {
                    bParameter = false;

                    editorLines[currentLineIndex].text = func;
                    currentLineIndex++;
                }
            }
        }
    }
}
