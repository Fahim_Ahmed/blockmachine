﻿using UnityEngine;
using UnityEngine.Events;

using TMPro;

public class BotController : MonoBehaviour {

    //public
    //public UnityEvent OnMoveComplete;

    public string BotId = "BOT_A";
    public float unitDistance = 5.12f;

    public int maxFloorCount = 2;
    public int currentFloor = 0;

    public TMP_Text consoleText;
    public int debugMove = 1;

    public bool isFlipped = false;
    public bool isOnExit = false;

    //public UnityEvent onResetComplete;
    public UnityEvent onFailed;

    //private
    private Transform raycaster;
    private Vector3 initialPosition;
    private bool initialPose;

    private string currentTag;
    private Vector2 slopeConn;

    
    void Awake()
    {
        raycaster = transform.Find("Raycaster");
        initialPosition = transform.position;
        initialPose = isFlipped;

        //onResetComplete = new UnityEvent();

        if (consoleText == null) consoleText = GameObject.FindGameObjectWithTag("Console").GetComponent<TMP_Text>();
    }

    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyUp(KeyCode.Space))
        {
            Move(debugMove);
        }

        if (Input.GetKeyUp(KeyCode.T)) Turn();
        if (Input.GetKeyUp(KeyCode.H)) Hit();

        if (Input.GetKeyUp(KeyCode.S)) DoScan();
        if (Input.GetKeyUp(KeyCode.P)) Reset();

#endif
        //Debug.DrawRay(raycaster.position, raycaster.forward*10);
    }

    //for now return only if it's walkable
    private bool CheckNextTile()
    {
        RaycastHit2D hit = Physics2D.Raycast(raycaster.position, raycaster.forward, 7);

        currentTag = "";
        slopeConn = new Vector2(-1, -1);

        if (hit.collider == null) return false;

        switch (hit.collider.tag)
        {
            case "Walkable":
            case "Switch":
                currentTag = hit.collider.tag;
                return true;
            case "Slope":
                slopeConn = hit.collider.GetComponent<SlopeProfile>().connectedFloor;

                bool res = (currentFloor == (int) slopeConn.x);

                if (isFlipped) res = (currentFloor == (int)slopeConn.y);

                if (res) currentTag = hit.collider.tag;
                return res;
        }

        return false;
    }

    private void MoveToNext(string tag)
    {
        //consoleText.text = "> " + slopeConn.ToString();

        switch (tag)
        {
            case "Walkable":
            case "Switch":
                if (!isFlipped) transform.position += new Vector3(unitDistance, 0);
                else transform.position -= new Vector3(unitDistance, 0);
                break;
            case "Slope":
                if (!isFlipped)
                {
                    //Going up Forward
                    if (slopeConn.y > currentFloor && slopeConn.y < maxFloorCount)
                    {
                        transform.position += new Vector3(unitDistance * 3, unitDistance * 2);

                        currentFloor = (int)slopeConn.y;
                        return;
                    }

                    //Going Down Forward
                    if (slopeConn.y < currentFloor && slopeConn.y >= 0)
                    {
                        transform.position += new Vector3(unitDistance * 3, -unitDistance * 2);

                        currentFloor = (int)slopeConn.y;
                        return;

                    }
                }
                else
                {
                    //Going Up Backward
                    if (slopeConn.x > currentFloor && slopeConn.x < maxFloorCount)
                    {
                        transform.position -= new Vector3(unitDistance * 3, -unitDistance * 2);

                        currentFloor = (int)slopeConn.x;
                        return;
                    }

                    //Going Down Backward
                    if (slopeConn.x < currentFloor && slopeConn.x >= 0)
                    {
                        transform.position -= new Vector3(unitDistance * 3, unitDistance * 2);

                        currentFloor = (int)slopeConn.x;
                    }
                }
                break;
        }
    }

    //public functions
    public bool Move(int unitSpace)
    {
        for(int i = 0; i < unitSpace; i++)
        {
            bool bSuccess = CheckNextTile();

            if (bSuccess)
            {
                MoveToNext(currentTag);
                print("Executed move()");
            }
            else
            {
                //Debug.Log("Obstacle");
                print("Failed to execute.");

                onFailed.Invoke();

                return false;
            }
        }


        return true;
    }

    public void Turn()
    {
        transform.rotation = Quaternion.Euler(0, transform.eulerAngles.y + 180, 0);
        isFlipped = !isFlipped;

        print("Executed turn()");
    } 
    
    public void Hit()
    {
        Animator anim = GetComponentInChildren<Animator>();

        if (anim != null) anim.SetTrigger("Hit");

        print("Beating completed.");
    }

    public void DoScan()
    {
        Animator anim = gameObject.GetComponentInChildren<Animator>();

        if (anim != null)
        {
            anim.SetTrigger("Scan");
        }

        bool res = CheckNextTile();
        if(res && currentTag == "Switch")
        {
            GetComponent<SwitchController>().switchPressed();
        }
        

        print("Scan completed");
    }

    public void Reset()
    {
        transform.position = initialPosition;
        if (initialPose != isFlipped) Turn();

        FindObjectOfType<CommandCompilerScript>().ResetAll();

        if(FindObjectOfType<SwitchController>() != null) FindObjectOfType<SwitchController>().SwReset();

        Debug.Log("test");
       // onResetComplete.Invoke();
    }

    public void print(string m)
    {
        consoleText.text = "> " + m;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.name == "Door")
        {
            print("Exit reached.");
            isOnExit = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isOnExit = false;
    }
}
