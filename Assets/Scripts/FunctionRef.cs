﻿using UnityEngine;

public class FunctionRef : MonoBehaviour {

    public string func;

    public EditorController editorController;

    private void Awake()
    {
        func = transform.GetChild(0).GetComponent<TMPro.TMP_Text>().text.ToLower();
        GetComponent<UnityEngine.UI.Button>().onClick.AddListener(() => editorController.OnButtonPress(func));        
    }
}
