﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadSceneScript : MonoBehaviour {

    public static int scnIdx = 0;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void LoadScene(int scn)
    {
        //UnityEngine.SceneManagement.SceneManager.LoadScene(scn);
        scnIdx++;
        UnityEngine.SceneManagement.SceneManager.LoadScene(scn);
    }

    public void LoadNext()
    {
        scnIdx++;
        UnityEngine.SceneManagement.SceneManager.LoadScene(scnIdx);
    }
}
