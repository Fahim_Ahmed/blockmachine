﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BotCommandEnum
{
    Null = 0,

    //Conditional commands
    Loop = 1,
    If,

    //Common commands
    Move = 11,
    Turn,
    Wait,
    

    //For Robot type A
    ScanFront = 101,

    //For Robot type B
    Hammer = 201,
}

public enum BotTypeEnum
{
    BotA,
    BotB,
}
