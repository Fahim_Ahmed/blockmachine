﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotBrain : MonoBehaviour
{
    [SerializeField]private BotTypeEnum _type;
    public int StartEnergy = 100;

    private Transform _mTrans;
    private BotController _mController;
    private List<BotCommand> _cmdList;
    private int _cmdIndex;
    private int _energy;

    void Awake()
    {
        _mTrans = this.transform;
    }
    // Use this for initialization
	void Start ()
	{
	    GameObject.FindGameObjectWithTag("Compiler")
	        .GetComponent<CommandCompilerScript>().OnPulse += ActionOnPulse;

        _cmdList = new List<BotCommand>();
	    _mController = _mTrans.GetComponent<BotController>();

        _energy = StartEnergy;
	}
	
    void ActionOnPulse()
    {
        if(_cmdIndex >= _cmdList.Count) return;

        var crntCommand = _cmdList[_cmdIndex];
        var cmdInt = (int) crntCommand.MainCommand;
        BotCommand callback = null;

        if (cmdInt < 10)
        {
            if (crntCommand.MainCommand == BotCommandEnum.Loop)
            {
                Loop(
                    crntCommand.ExtraCommands[0].MainCommand, 
                    crntCommand.valueParam);
                
            }
            else if (crntCommand.MainCommand == BotCommandEnum.If)
            {
                IfCondition(
                    crntCommand.ExtraCommands[0].MainCommand);
                    callback = crntCommand.ExtraCommands[1];
            }

            crntCommand = _cmdList[_cmdIndex];
        }

        switch (crntCommand.MainCommand)
        {
            case BotCommandEnum.Move:
                Move();
                break;
            case BotCommandEnum.Turn:
                Turn();
                break;
            case BotCommandEnum.Wait:
                Wait();
                break;
            case BotCommandEnum.ScanFront:
                if(ScanFront() && callback != null)
                    IfCallback(callback);
                break;
            case BotCommandEnum.Hammer:
                Hammer();
                break;
        }


        _cmdIndex++;
        if(_cmdIndex >= _cmdList.Count)
            Debug.Log(_mTrans.name+": Reached End of Command List");
    }

    //public functions
    public void AddCommand(BotCommand command)
    {
        var cmdValue = (int) command.MainCommand;
        if(cmdValue > 0 && cmdValue < 100)
            _cmdList.Add(command);
        else if (cmdValue < 200 && _type == BotTypeEnum.BotA)
        {
            //BotA special commands
            _cmdList.Add(command);
        }
        else if (cmdValue > 200 && cmdValue < 300 && _type == BotTypeEnum.BotB)
        {
            //BotB special commands
            _cmdList.Add(command);
        }
    }

    public void ResetBot()
    {
        _cmdList.Clear();
        _cmdIndex = 0;
        _energy = StartEnergy;
    }

    public bool EndOfCommands()
    {
        if (_cmdList.Count > 0 && _cmdIndex < _cmdList.Count)
            return false;
        return true;
    }

    public int GetEnergy()
    {
        return _energy;
    }

    //private functions
    private void Move()
    {
        _energy -= 5;
        _mController.Move(1);
        Debug.Log(_mTrans.name+": Moved");
    }

    private void Turn()
    {
        _energy -= 10;
        _mController.Turn();
        Debug.Log(_mTrans.name + ": Turned");
    }

    private void Wait()
    {
        _energy -= 0;
        Debug.Log(_mTrans.name + ": Waited");
    }

    private bool ScanFront()
    {
        if(_type != BotTypeEnum.BotA)
            return false;

        _energy -= 20;
        _mController.DoScan();

        Debug.Log(_mTrans.name + ": Scanned infront");
        return true;
    }

    private void Hammer()
    {
        if (_type != BotTypeEnum.BotB)
            return;
        _energy -= 30;
        Debug.Log(_mTrans.name + ": Hammer time");
    }

    private void Loop(BotCommandEnum command, int count)
    {
        _energy -= 10;

        _cmdList.RemoveAt(_cmdIndex);
        for (int i = 0; i < count; i++)
        {
            if (_cmdList.Count - 1 == _cmdIndex)
                _cmdList.Add(new BotCommand(-1, command));
            else
                _cmdList.Insert(_cmdIndex, new BotCommand(-1, command));
        }
    }

    private void IfCondition(BotCommandEnum condition)
    {
        _energy -= 10;
        _cmdList.RemoveAt(_cmdIndex);
        if(_cmdList.Count == _cmdIndex)
            _cmdList.Add(new BotCommand(-1, condition));
        else _cmdList.Insert(_cmdIndex, new BotCommand(-1, condition));
    }

    private void IfCallback(BotCommand command)
    {
        if (_cmdList.Count  == _cmdIndex+1)
            _cmdList.Add(command);
        else _cmdList.Insert(_cmdIndex+1, command);
    }

}
