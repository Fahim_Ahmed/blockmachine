﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScanEventReceiver : MonoBehaviour {

	public void AnimEndEvent()
    {
        FindObjectOfType<CommandCompilerScript>().isAnimPlaying = false;
        FindObjectOfType<CommandCompilerScript>().Update();
    }

    public void AnimStartEvent()
    {
        FindObjectOfType<CommandCompilerScript>().isAnimPlaying = true;
    }
}
