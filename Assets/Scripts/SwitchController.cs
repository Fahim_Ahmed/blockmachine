﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class SwitchController : MonoBehaviour {

    public Color red;
    public Color blue;

    public SpriteRenderer swRen;
    public GameObject block;

    //public UnityEngine.Events.UnityEvent onSwitchTrigger;

	// Use this for initialization
	void Start () {
		
	}
	
	public void switchPressed()
    {
        swRen.color = blue;
        block.SetActive(false);
        //onSwitchTrigger.Invoke();
    }

    public void SwReset()
    {
        swRen.color = red;
        block.SetActive(true);
        //onSwitchTrigger.Invoke();
    }
}
