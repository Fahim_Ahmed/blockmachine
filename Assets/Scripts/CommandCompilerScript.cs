﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CommandCompilerScript : MonoBehaviour
{
    public BotBrain BotA, BotB;
    public EditorController editorA, editorB;

    public string TestInputA, TestInputB;
    public float PulseTime = 1.0f;

    public delegate void PulseAction();

    public event PulseAction OnPulse;

    public bool isAnimPlaying = false;

    //private variables
    //private List<BotCommandSet> _cmdList;
    private float _pulseDelta;
    private bool _isPlaying;

    private TMP_InputField[] _eLinesBotA, _eLinesBotB;
    private TMPro.TMP_Text eConsole;

    //custom classes
    public class BotCommandSet
    {
        public int BotId;
        public BotCommandEnum Command;

        public BotCommandSet(int botId, BotCommandEnum cmd)
        {
            BotId = botId;
            Command = cmd;
        }
    }

    // Use this for initialization
    void Start()
    {
        //Debug.Log(TestInputB.Substring(5, TestInputB.IndexOf(",", StringComparison.Ordinal) - 5));

        if (BotA == null)
        {
            var bot = GameObject.Find("BotA");
            if(bot != null)
                BotA = bot.GetComponent<BotBrain>();
        }


        if (BotB == null)
        {
            var bot = GameObject.Find("BotB");
            if (bot != null)
                BotB = bot.GetComponent<BotBrain>();
        }
        
        var panelSwitch = FindObjectOfType<PanelSwitcher>();
        _eLinesBotA = panelSwitch.panelA.GetComponent<EditorController>().editorLines;
        _eLinesBotB = panelSwitch.panelB.GetComponent<EditorController>().editorLines;

        _pulseDelta = 0.0f;
        _isPlaying = false;

        eConsole = GameObject.FindGameObjectWithTag("energyConsole").GetComponent<TMP_Text>();
    }

    // Update is called once per frame
    public void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ParseCommand(TestInputA, TestInputB);
        }
        else if (Input.GetKeyDown(KeyCode.R))
        {
            Debug.Log("Compiler: Stopped Pulse");
            ResetAll();
        }
#endif


        if (_isPlaying)
        {

            if (_pulseDelta > 0)
            {
                _pulseDelta -= Time.deltaTime;
            }
            else
            {
               // if (isAnimPlaying) return;

                var stop = true;
                if (BotA != null)
                    stop = BotA.EndOfCommands();
                
                if (stop && BotB != null)
                    stop = BotB.EndOfCommands();

                if (!stop)
                {
                    if (OnPulse != null)
                    {
                        Debug.Log("------PULSE-----");
                        OnPulse();

                        eConsole.text = ">";

                        if(BotA != null)
                        {
                            eConsole.text += " BotA Energy: " + BotA.GetEnergy();
                        }

                        if (BotB != null)
                        {
                            eConsole.text += " BotB Energy: " + BotB.GetEnergy();
                        }
                    }
                    _pulseDelta = PulseTime;
                }
                else
                {
                    CheckLevelComplete();
                    _isPlaying = false;
                }
            }
        }
    }

    //public functions
    public void ParseCommand()
    {
        var lineCount = _eLinesBotA.Length;
        for (int i = 0; i < lineCount; i++)
        {
            if (BotA != null)
            {
                var cmdText = _eLinesBotA[i].text;
                if (!string.IsNullOrEmpty(cmdText))
                    AddCommand(BotA, cmdText);
            }

            if (BotB != null)
            {
                var cmdText = _eLinesBotB[i].text;
                if (!string.IsNullOrEmpty(cmdText))
                    AddCommand(BotB, cmdText);
            }
        }
        _isPlaying = true;
    }

    public void ParseCommand(string cmdBotA, string cmdBotB)
    {
        ResetAll();

        //Process string for BotA
        if (BotA != null && !string.IsNullOrEmpty(cmdBotA))
        {
            var instructions = cmdBotA.Split(';');
            foreach (var instruction in instructions)
            {
                AddCommand(BotA, instruction);
            }
        }

        //Process string for BotB
        if (BotB != null && !string.IsNullOrEmpty(cmdBotB))
        {
            var instructions = cmdBotB.Split(';');
            foreach (var instruction in instructions)
            {
                AddCommand(BotB, instruction);
            }
        }

        _isPlaying = true;
        
    }

    public void ResetAll()
    {
        _pulseDelta = 0.0f;
        _isPlaying = false;

        if(BotA != null) BotA.ResetBot();
        if(BotB != null) BotB.ResetBot();
        Debug.Log("Compiler: Reset All");
    }

    //private functions
    private void CheckLevelComplete()
    {
        bool isLevelComplete = false;

        if(BotA != null)
        {
            if (BotA.GetComponent<BotController>().isOnExit)
            {
                //level complete
                isLevelComplete = true;
            }
            else
            {
                editorA.ClearAll();
                BotA.GetComponent<BotController>().print("Execution failed.");
                eConsole.text = ">";
            }
        }

        if (isLevelComplete && BotB != null)
        {
            if (BotB.GetComponent<BotController>().isOnExit) {
                isLevelComplete = true;
            }
            else
            {
                editorB.ClearAll();
                BotB.GetComponent<BotController>().print("Execution failed.");
            }
        }

        if (isLevelComplete)
        {
            //Go next;
            FindObjectOfType<LoadSceneScript>().LoadNext();
        }
        else
        {
            if (BotA != null) editorA.ClearAll();
            if (BotB != null) editorB.ClearAll();
        }
    }

    private bool AddCommand(BotBrain bot, string cmdString)
    {
        cmdString = cmdString.ToLower();
        if (cmdString.Contains("loop("))
        {
            var cmIndex = cmdString.IndexOf(",", StringComparison.Ordinal);
            var count = int.Parse(cmdString.Substring(5, cmIndex - 5));
            var command = GetCommandEnum(cmdString[cmIndex + 2]);
            if (command == BotCommandEnum.Null) return false;
            bot.AddCommand(new BotCommand(-1,command , count)); 
        }
        else if (cmdString.Contains("if("))
        {
            var condition = GetCommandEnum(cmdString[cmdString.IndexOf("(") + 1]);
            if (condition == BotCommandEnum.Null) return false;
            var command = GetCommandEnum(cmdString[cmdString.IndexOf(")") + 2]);
            if (command == BotCommandEnum.Null) return false;
            bot.AddCommand(new BotCommand(-1,condition, command));
        }
        else
        {
            var command = GetCommandEnum(cmdString[0]);
            if (command == BotCommandEnum.Null) return false;
            bot.AddCommand(new BotCommand(-1, command));
        }
        return true;
    }

    private BotCommandEnum GetCommandEnum(char cmdChar)
    {
        BotCommandEnum returnVal;
        switch (cmdChar)
        {
            case 'm':
                returnVal = BotCommandEnum.Move;
                break;
            case 't':
                returnVal = BotCommandEnum.Turn;
                break;
            case 'w':
                returnVal = BotCommandEnum.Wait;
                break;
            case 's':
                returnVal = BotCommandEnum.ScanFront;
                break;
            case 'h':
                returnVal = BotCommandEnum.Hammer;
                break;
            default:
                returnVal = BotCommandEnum.Null;
                break;
        }
        return returnVal;
    }
}

[Serializable]
public class BotCommand
{
    public BotCommandEnum MainCommand;
    public List<BotCommand> ExtraCommands;
    public int LineId;
    public int valueParam;

    public BotCommand(int lineId, BotCommandEnum command)
    {
        MainCommand = command;
        LineId = lineId;
    }

    public BotCommand(int lineId, BotCommandEnum command, int count)
    {
        MainCommand = BotCommandEnum.Loop;
        ExtraCommands = new List<BotCommand>(1) {new BotCommand(lineId, command)};
        valueParam = count;

        LineId = lineId;
    }

    public BotCommand(int lineId, BotCommandEnum condition, BotCommandEnum command)
    {
        MainCommand = BotCommandEnum.If;
        ExtraCommands = new List<BotCommand>(2)
        {
            new BotCommand(lineId, condition),
            new BotCommand(lineId, command)
        };
    }
}
